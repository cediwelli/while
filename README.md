# WHILE Programming-Language Interpreter

This is a WHILE interpreter, it is designed to be used as a library but there is also a CLI for
directly interpreting WHILE Programs. The File-Extensions are irrelevant but I usally call them `.while`.

## Language Specifiactions

The Interpreter can understand both the WHILE and the LOOP languages.

### Keywords

`WHILE`, `LOOP`, `DO`, `END`, `IF`, `THEN`, `ELSE`

### Operations

`+`, `-`, `*`, `DIV`, `MOD`, `:=`, `=`, `!=`, `>`, `<`, `>=`, `<=`

### Notes

- The semi-colons `;` are actually completly optional. They are fully ignored.
- You can write unspaced-onliners like `WHILEx1>0DOx1:=x1-1END`
- Currently you can only use the variables `x0` to `x99`

## Installation

### Dependencies

Since the 22.02.2022, the Library is dependant on the `datastructures-c`-Library,
which has to be installed.

To install the Library, go to [https://gitlab.com/cediwelli/datastructures-c](https://gitlab.com/cediwelli/datastructures-c)
and install as described. After that, follow the description below:

1. `git clone https://gitlab.com/cediwelli/while.git && cd while`
2. `make && sudo make install`
3. `make cli` (To build the CLI, optional if you only want the library)

## Usage

`whilecli [OPTIONS] [FILE] [INPUT VALUES]`

```
OPTIONS

	-all
		at the end of the execution, show all
		variables which are non-zero.

INPUT VALUES

	A list of n space-separated integers which will be
	assigned to x1...xn

FILE

	The file containing the Code.
```

## Linking

After installing with `sudo make install`, you can link with `-lwhile`.
Single relevant header-file: `#include <while/while.h>`

## API

```c

/**
* Set the value of a variable.
* Can be used to initilize input variables.
* @param var The "index" of the variable.
* @param val The new value.
*/
void while_setVariable(int var, int val);

/**
* Get a variable xn where var=n.
* @param var The "index" of the variable.
* @return The Value of the variable.
*/
int while_getVariable(int var);

/**
* Runs a WHILE-Program.
* @param The string that contains the WHILE-Program.
*/
int while_run(char * program);

```

## Changes

### 22.02.2022
- It is no longer possible to use references (xx10 doesn't work anymore).
- Halting functionality removed.
- Runtime decreased dramatically. Because of previous programming mistakes,
	the code ran pretty slow. The Runtime is now reasonable.
- Now dependant on the `datastructures-c`-Library.