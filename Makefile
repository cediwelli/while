CC=gcc
FILES=$(wildcard src/*.c)
OBJECTS=$(patsubst src/%.c,bin/%.o,$(FILES))

LIBNAME=while
LIBOUT=lib$(LIBNAME).so
INCLUDES=-Iinclude/

all: bin/while.o
	$(CC) bin/while.o -fPIC -shared -ldatastructures -o bin/$(LIBOUT)

bin/%.o: src/%.c
	$(CC) -c -fPIC $< $(INCLUDES) -o $@

cli:
	$(CC) src/main.c -lwhile -o bin/whilecli

install:
	mkdir /usr/include/$(LIBNAME)
	cp $(wildcard include/*) /usr/include/$(LIBNAME)
	cp bin/$(LIBOUT) /usr/lib/

remove:
	rm -r /usr/include/$(LIBNAME)
	rm /usr/lib/$(LIBOUT)
