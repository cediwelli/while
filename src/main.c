
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <while/while.h>

int show_all_variables = 0;
int step = 0;

char * read_file(char * filename) {

	int filefd = open(filename, O_RDONLY);

	int filelen = lseek(filefd, 0, SEEK_END);
	lseek(filefd, 0, SEEK_SET);

	char * buf = (char *) malloc(filelen + 1);
	read(filefd, buf, filelen);
	buf[filelen] = '\0';

	close(filefd);

	return buf;
}

void print_all_vars() {
	for(int i = 1; i < MAXVARS; ++i) {

		int var_val = while_getVariable(i);

		if(var_val != 0) {
			printf("(x%d) = %d\n", i, var_val);
		}
	}
}

/*void haltfunction() {

	int token_type, token_value;

	while_getCurrentToken(&token_type, &token_value);

	printf("Current Token: ");

	switch(token_type) {
		case WHILE_TOKEN_KEYWORD:
			printf("%s", keywords[token_value]);
			break;
		case WHILE_TOKEN_OPERATOR:
			printf("%s", operators[token_value]);
			break;
		case WHILE_TOKEN_VARIABLE:
			printf("x%d\n", token_value);
			break;
		case WHILE_TOKEN_CONSTANT:
			printf("%d", token_value);
			break;
		case WHILE_TOKEN_SEPARATOR:
			printf(";");
			break;
	}

	printf("\n");

	print_all_vars();

	while(getchar() != '\n');
}*/

int parse_options(int argc, char ** argv) {

	int i = 0;
	for(; i < argc; ++i) {

		char * arg = argv[i];

		if(arg[0] != '-') {
			return i+1;
		}

		if(strcmp("step", &arg[1]) == 0) {
			//while_setHaltFunction(haltfunction);
			step = 1;
		} else if(strcmp("all", &arg[1]) == 0) {
			show_all_variables = 1;
		} else if(strcmp("help", &arg[1]) == 0) {
			printf("Usage: whilecli [OPTIONS] file\n");
			printf("\n    -help    prints this message.\n");
			printf("\n    -step    executes code step by step.\n");
			printf("\n    -all     shows content of all variables at the end.\n\n");
			exit(0);
		}
	}

	return i+1;
}

void parse_input(int argc, char ** argv) {

	for(int i = 0; i < argc; ++i) {

		char * arg = argv[i];

		int x = atoi(arg);
		while_setVariable(i+1, x);
		printf("input (x%d) = %d\n", i+1, x);
	}

	printf("\n");
}

int main(int argc, char ** argv) {

	int argi = parse_options(argc-1, &argv[1]);

	if(argc - argi <= 0) {
		printf("Missing File.\n");
		return -1;
	}

	char * inpfile = argv[argi++];
	char * file_contents = read_file(inpfile);

	printf("---- %s\n", inpfile);

	if(argc - argi > 0) {
		parse_input(argc-argi, &argv[argi]);
	}

	while_run(file_contents);

	if(step)
		printf("---- %s END\n", inpfile);

	printf("output (x0) = %d\n", while_getVariable(0));

	if(show_all_variables) {
		printf("\n");
		print_all_vars();
	}

	free(file_contents);
	return 0;
}
