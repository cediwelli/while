#include "while.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <datastructures/array.h>

static int runSubroutine(array * tokens, int indexStart, int indexEnd);

char * keywords[] = {
	"WHILE", "LOOP", "DO", "END", "IF", "THEN", "ELSE"
};

char * operators[] = {
	[WHILE_PLUS]  = "+",
	[WHILE_MINUS] = "-",
	[WHILE_MUL]   = "*",
	[WHILE_DIV]   = "DIV",
	[WHILE_MOD]   = "MOD",
	[WHILE_ASGN]  = ":=",
	[WHILE_EQL]   = "=",
	[WHILE_LEQ]   = "<=",
	[WHILE_LESS]  = "<",
	[WHILE_GEQ]   = ">=",
	[WHILE_GRT]   = ">",
	[WHILE_NEQ]   = "!="
};

static int variables[MAXVARS] = {0};

typedef struct {
	tokentype_e t_type;
	int         t_value;
} token_t;

typedef struct {
	token_t * operand1;
	token_t * operand2;
	token_t * operator;
} expr_t;

static inline int isToken(token_t * tok, tokentype_e type, int value) {
	if (tok == NULL) {
		return 0;
	}
	return tok->t_type==type && tok->t_value==value;
}

static inline int isTokenType(token_t * tok, tokentype_e type) {
	if (tok == NULL) {
		return 0;
	}
	return tok->t_type==type;
}

/**
 * Will get the index at which the corresponding END or ELSE lies.
 * Should be started with startIndex pointing to DO or THEN.
 * The returned Index will be the index of the corresponsing END or ELSE.
 * */
static int getSubroutine(array * tokens, int startIndex, int countElse) {
	int len  = array_getLength(tokens);
	int open = 1;

	while (startIndex < len) {
		token_t * tok = array_get(tokens, startIndex);

		if (isToken(tok, WHILE_TOKEN_KEYWORD, WHILE_END)) {
			--open;
		} else if (isToken(tok, WHILE_TOKEN_KEYWORD, WHILE_DO) ||
				isToken(tok, WHILE_TOKEN_KEYWORD, WHILE_THEN)) {
			++open;
		}

		if (open <= 0 ||
				(countElse && open <= 1 && isToken(tok, WHILE_TOKEN_KEYWORD,
						WHILE_ELSE))) {
			break;
		}

		++startIndex;
	}

	return startIndex;
}

static int getExpr(array * tokens, int startIndex, expr_t * out) {
	token_t * tok1 = array_get(tokens, startIndex);
	token_t * tok2 = array_get(tokens, startIndex+1);
	token_t * tok3 = array_get(tokens, startIndex+2);

	if (tok1 == NULL) {
		return -1;
	}

	if (tok1->t_type != WHILE_TOKEN_CONSTANT     &&
			tok1->t_type != WHILE_TOKEN_VARIABLE) {
		return -1;
	}

	if (tok2 == NULL || tok2->t_type != WHILE_TOKEN_OPERATOR) {
		out->operand1 = tok1;
		out->operand2 = NULL;
		out->operator = NULL;
		return 1;
	}

	if (tok3 == NULL || (tok3->t_type != WHILE_TOKEN_CONSTANT &&
			tok3->t_type != WHILE_TOKEN_VARIABLE)) {
		return -1;
	}

	out->operand1 = tok1;
	out->operand2 = tok3;
	out->operator = tok2;
	return 3;
}

static int getTokenValue(token_t * tok) {
	if (tok == NULL) {
		return 0;
	} else if (tok->t_type == WHILE_TOKEN_CONSTANT) {
		return tok->t_value;
	} else if (tok->t_type == WHILE_TOKEN_VARIABLE) {
		return variables[tok->t_value];
	}
	return -1;
}

static int evalCondition(expr_t * expr) {
	int val1 = getTokenValue(expr->operand1);
	int val2 = getTokenValue(expr->operand2);
	switch (expr->operator->t_value) {
		case WHILE_EQL:
			return val1 == val2;
		case WHILE_NEQ:
			return val1 != val2;
		case WHILE_GEQ:
			return val1 >= val2;
		case WHILE_GRT:
			return val1 > val2;
		case WHILE_LEQ:
			return val1 <= val2;
		case WHILE_LESS:
			return val1 < val2;
	}
	return -1;
}

static int evalArithmetic(expr_t * expr) {
	int val1 = getTokenValue(expr->operand1);
	int val2 = getTokenValue(expr->operand2);
	if (expr->operator == NULL) {
		return val1+val2;
	}
	switch (expr->operator->t_value) {
		case WHILE_MINUS: {
			int r = val1 - val2;
			return (r>=0)*r + (r<0)*0;
		}
		case WHILE_MUL:
			return val1 * val2;
		case WHILE_DIV:
			return val1/val2;
		case WHILE_MOD:
			return val1%val2;
		default:
			return val1+val2;
	}
}

static int whileloop(array * tokens, int startIndex) {

	expr_t condExpr;
	int    rv;
	int    subroutineEnd;
	int    index = startIndex;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_WHILE)) {
		printf("WHILE: Expected WHILE-Keyword at %d!\n", index);
		return -1;
	}
	++index;

	if ((rv = getExpr(tokens, index, &condExpr)) == -1) {
		printf("WHILE: Expected a valid conditional expression at %d!\n", index);
		return -1;
	}
	index += rv;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_DO)) {
		printf("WHILE: Expected DO-Keyword at %d!\n", index);
		return -1;
	}
	++index;
	subroutineEnd = getSubroutine(tokens, index, 0);

	if (!isToken(array_get(tokens, subroutineEnd),
			WHILE_TOKEN_KEYWORD, WHILE_END)) {
		printf("WHILE: Expected END-Keyword at %d!\n", index);
		return -1;
	}

	if (subroutineEnd == -1) {
		printf("WHILE: Expected valid Subroutine at %d!\n", index);
		return -1;
	}

	while (evalCondition(&condExpr) == 1) {
		runSubroutine(tokens, index, subroutineEnd);
	}
	return subroutineEnd-startIndex;
}

static int forloop(array * tokens, int startIndex) {

	int loopCount = 0;
	int rv;
	int subroutineEnd;
	int index = startIndex;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_LOOP)) {
		printf("LOOP: Expected LOOP-Keyword at %d!\n", index);
		return -1;
	}
	++index;

	if ((rv = getTokenValue(array_get(tokens, index))) == -1) {
		printf("LOOP: Expected a CONSTANT or VARIABLE as loop counter at %d!\n",
				index);
		return -1;
	}
	++index;
	loopCount = rv;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_DO)) {
		printf("LOOP: Expected DO-Keyword at!\n", index);
		return -1;
	}
	++index;
	subroutineEnd = getSubroutine(tokens, index, 0);

	if (!isToken(array_get(tokens, subroutineEnd),
			WHILE_TOKEN_KEYWORD, WHILE_END)) {
		printf("LOOP: Expected END-Keyword at %d!\n", index);
		return -1;
	}

	for (int i = 0; i < loopCount; ++i) {
		runSubroutine(tokens, index, subroutineEnd);
	}
	return subroutineEnd - startIndex;
}

static int ifClause(array * tokens, int startIndex) {

	expr_t condExpr;
	int    rv;
	int    subroutineEnd;
	int    elseSubroutineEnd = 0;
	int    hasElse           = 0;
	int    index             = startIndex;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_IF)) {
		printf("IF: Expected IF-Keyword at %d!\n", index);
		return -1;
	}
	++index;

	if ((rv = getExpr(tokens, index, &condExpr)) == -1) {
		printf("IF: Expected a valid conditional expression at %d!\n", index);
		return -1;
	}
	index += rv;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_KEYWORD, WHILE_THEN)) {
		printf("IF: Expected THEN-Keyword at %d!\n", index);
		return -1;
	}
	++index;
	subroutineEnd = getSubroutine(tokens, index, 1);

	if (isToken(array_get(tokens, subroutineEnd),
			WHILE_TOKEN_KEYWORD, WHILE_ELSE)) {
		hasElse           = 1;
		elseSubroutineEnd = getSubroutine(tokens, index, 0);

		if (!isToken(array_get(tokens, elseSubroutineEnd),
				WHILE_TOKEN_KEYWORD, WHILE_END)) {
			printf("IF: Expected END-Keyword at %d!\n", elseSubroutineEnd);
			return -1;
		}
	} else {
		if (!isToken(array_get(tokens, subroutineEnd),
				WHILE_TOKEN_KEYWORD, WHILE_END)) {
			printf("IF: Expected END- or ELSE-Keyword at %d!\n", subroutineEnd);
			return -1;
		}
	}

	if (evalCondition(&condExpr)) {
		runSubroutine(tokens, index, subroutineEnd);
	} else if (hasElse) {
		runSubroutine(tokens, subroutineEnd+1, elseSubroutineEnd);
	}
	return ((hasElse)*elseSubroutineEnd + (!hasElse)*subroutineEnd) - startIndex;
}

static int assignment(array * tokens, int startIndex) {

	int       rv;
	int       index = startIndex;
	expr_t    arithExpr;
	token_t * lval;

	lval = array_get(tokens, index);
	if (!isTokenType(lval, WHILE_TOKEN_VARIABLE)) {
		printf("\":=\": Expected lvalue at %d\n", index);
		return -1;
	}
	++index;

	if (!isToken(array_get(tokens, index), WHILE_TOKEN_OPERATOR, WHILE_ASGN)) {
		printf("\":=\": Expected assignment operator (:=) at %d!\n", index);
		return -1;
	}
	++index;

	if ((rv = getExpr(tokens, index, &arithExpr)) == -1) {
		printf("\":=\": Expected a valid arithmetic expression at %d!\n", index);
		return -1;
	}
	index += rv;
	rv     = evalArithmetic(&arithExpr);
	variables[lval->t_value] = rv;

	return index - startIndex;
}

static int runSubroutine(array * tokens, int indexStart, int indexEnd) {
	while (indexStart < indexEnd) {
		token_t * currentToken = array_get(tokens, indexStart);
		int       rv;

		if(currentToken->t_type == WHILE_TOKEN_KEYWORD) {
				switch(currentToken->t_value) {
					case WHILE_WHILE:
						if ((rv = whileloop(tokens, indexStart)) == -1) {
							return -1;
						}
						indexStart += rv;
						break;

					case WHILE_LOOP:
						if ((rv = forloop(tokens, indexStart)) == -1) {
							return -1;
						}
						indexStart += rv;
						break;

					case WHILE_IF:
						if ((rv = ifClause(tokens, indexStart)) == -1) {
							return -1;
						}
						indexStart += rv;
						break;

					case WHILE_END:
						indexStart += 1;
						break;
				}

			} else if(currentToken->t_type == WHILE_TOKEN_VARIABLE) {
				if ((rv = assignment(tokens, indexStart)) == -1) {
						return -1;
				}
				indexStart += rv;

			} else if(currentToken->t_type == WHILE_TOKEN_SEPARATOR) {
				indexStart += 1;
			}
	}
	return indexStart;
}

static int startsWithKeyword(char * str, int keyword) {

	char * keyword_str = keywords[keyword];
	int    keyword_len = strlen(keyword_str);

	if(strncmp(str, keyword_str, keyword_len) == 0) {
		return keyword_len;
	}
	return 0;
}

static int startsWithOperator(char * str, int operator) {

	char * operator_str = operators[operator];
	int    operator_len = strlen(operator_str);

	if(strncmp(str, operator_str, operator_len) == 0) {
		return operator_len;
	}
	return 0;
}

static int startsWithVariable(char * str, int * var_out) {

	int var = 0;
	int n   = 0;

	if(str[0] != 'x' || sscanf(&str[1], "%2d%n", &var, &n) != 1) {
		return 0;
	}

	*var_out = var;
	return n+1;
}

static int getArithmeticOperator(char * program, int * op_out) {

	int op_skip = 0;

	if((op_skip = startsWithOperator(program, WHILE_PLUS)) > 0) {
		*op_out = WHILE_PLUS;

	} else if((op_skip = startsWithOperator(program, WHILE_MINUS)) > 0) {
		*op_out = WHILE_MINUS;

	} else if((op_skip = startsWithOperator(program, WHILE_MUL)) > 0) {
		*op_out = WHILE_MUL;

	} else if((op_skip = startsWithOperator(program, WHILE_DIV)) > 0) {
		*op_out = WHILE_DIV;

	} else if((op_skip = startsWithOperator(program, WHILE_MOD)) > 0) {
		*op_out = WHILE_MOD;
	}

	return op_skip;
}

/* Technically binary operator, too...*/
static int getConditionalOperator(char * program, int * op_out) {
	int op_skip = 0;

	if((op_skip = startsWithOperator(program, WHILE_EQL)) > 0) {
		*op_out = WHILE_EQL;

	} else if((op_skip = startsWithOperator(program, WHILE_LEQ)) > 0) {
		*op_out = WHILE_LEQ;

	} else if((op_skip = startsWithOperator(program, WHILE_LESS)) > 0) {
		*op_out = WHILE_LESS;

	} else if((op_skip = startsWithOperator(program, WHILE_GEQ)) > 0) {
		*op_out = WHILE_GEQ;

	} else if((op_skip = startsWithOperator(program, WHILE_GRT)) > 0) {
		*op_out = WHILE_GRT;

	} else if((op_skip = startsWithOperator(program, WHILE_NEQ)) > 0) {
		*op_out = WHILE_NEQ;
	}

	return op_skip;
}

static int getKeyword(char * program, int * keyword_out) {

	int keyword_len = 0;

	if((keyword_len = startsWithKeyword(program, WHILE_WHILE)) > 0) {
		*keyword_out = WHILE_WHILE;

	} else if((keyword_len = startsWithKeyword(program, WHILE_LOOP)) > 0) {
		*keyword_out = WHILE_LOOP;

	} else if((keyword_len = startsWithKeyword(program, WHILE_DO)) > 0) {
		*keyword_out = WHILE_DO;

	} else if((keyword_len = startsWithKeyword(program, WHILE_END)) > 0) {
		*keyword_out = WHILE_END;

	} else if((keyword_len = startsWithKeyword(program, WHILE_IF)) > 0) {
		*keyword_out = WHILE_IF;

	} else if((keyword_len = startsWithKeyword(program, WHILE_THEN)) > 0) {
		*keyword_out = WHILE_THEN;

	} else if((keyword_len = startsWithKeyword(program, WHILE_ELSE)) > 0) {
		*keyword_out = WHILE_ELSE;
	}

	return keyword_len;
}

static int isWhite(char c) {
	return c==' ' || c=='\t' || c=='\n';
}

static int isNum(char c) {
	return c>='0'&&c<='9';
}

static int while_skipWhite(char * str) {
	int i = 0;
	for(; isWhite(str[i]); ++i);
	return i;
}

/**
* Returns the amount of characters skipped.
* Writes either the token into the token pointer or -1.
* When token_out == -1, the read token is a variable and its index
* is written to var_out.
*/
static int getToken(char * program, int * token_type, int * token_value) {

	int index = while_skipWhite(program);
	char first_char = program[index];

	if(first_char == 0) {
		*token_type = -1;
		*token_value = 0;
		return 1;
	}

	if(first_char == 'x') {

		int var;
		int varlen = startsWithVariable(&program[index], &var);

		if(varlen <= 0) {
			goto err;
		}

		*token_type = WHILE_TOKEN_VARIABLE;
		*token_value = var;
		return index + varlen;

	} else if(!isNum(first_char)) {

		int keyword;
		int keyword_len = getKeyword(&program[index], &keyword);

		if(keyword_len > 0) {
			*token_type = WHILE_TOKEN_KEYWORD;
			*token_value = keyword;
			return index + keyword_len;
		}

		keyword_len = getArithmeticOperator(&program[index], &keyword);

		if(keyword_len > 0) {
			*token_type = WHILE_TOKEN_OPERATOR;
			*token_value = keyword;
			return index + keyword_len;
		}

		keyword_len = getConditionalOperator(&program[index], &keyword);

		if(keyword_len > 0) {
			*token_type = WHILE_TOKEN_OPERATOR;
			*token_value = keyword;
			return index + keyword_len;
		}

		if(strncmp(":=", &program[index], 2) == 0) {
			*token_type = WHILE_TOKEN_OPERATOR;
			*token_value = WHILE_ASGN;
			return index + 2;
		}

		if(strncmp(";", &program[index], 1) == 0) {
			*token_type = WHILE_TOKEN_SEPARATOR;
			*token_value = 0;
			return index + 1;
		}

	} else {

		int d;
		int n = 0;
		int r = sscanf(&program[index], "%d%n", &d, &n);

		if(r <= 0) {
			goto err;
		}

		*token_type = WHILE_TOKEN_CONSTANT;
		*token_value = d;
		return index + n;
	}

	err:
	printf("Unkown token: %s\n", &program[index]);
	exit(0);

	return -1;
}

static int tokenizer(char * program, array * tokens) {

	int token_type;
	int token_value;
	int index = 0;

	while ((index += getToken(&program[index], &token_type, &token_value)) > 0 && token_type >= 0) {
		token_t token;

		token.t_type  = token_type;
		token.t_value = token_value;
		array_append(tokens, &token);
	}
	return 0;
}

int while_run(char * program) {

	int     tokenCount;
	array * tokens = array_create(sizeof(token_t));

	tokenizer(program, tokens);
	tokenCount = array_getLength(tokens);

	runSubroutine(tokens, 0, tokenCount);
	return 0;
}

int while_getVariable(int var) {
	if(var < MAXVARS) {
		return variables[var];
	}
	return -1;
}

void while_setVariable(int var, int val) {
	if(var < MAXVARS) {
		variables[var] = val;
	}
}
