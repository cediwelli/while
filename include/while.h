#ifndef WHILE_H_
#define WHILE_H_

#define MAXVARS 100

typedef enum {
	WHILE_TOKEN_KEYWORD,
	WHILE_TOKEN_OPERATOR,
	WHILE_TOKEN_VARIABLE,
	WHILE_TOKEN_CONSTANT,
	WHILE_TOKEN_SEPARATOR
} tokentype_e;

typedef enum {
	WHILE_WHILE,
	WHILE_LOOP,
	WHILE_DO,
	WHILE_END,
	WHILE_IF,
	WHILE_THEN,
	WHILE_ELSE,
	WHILE_KEYWORD_COUNT
} keyword_e;

typedef enum {
	WHILE_PLUS,
	WHILE_MINUS,
	WHILE_MUL,
	WHILE_DIV,
	WHILE_MOD,
	WHILE_ASGN,
	WHILE_EQL,
	WHILE_LESS,
	WHILE_LEQ,
	WHILE_GRT,
	WHILE_GEQ,
	WHILE_NEQ
} operator_e;

void while_getCurrentToken(int * token_type, int * token_value);

/**
* Set the value of a variable.
* Can be used to initilize input variables.
* @param var The "index" of the variable.
* @param val The new value.
*/
void while_setVariable(int var, int val);

/**
* Get a variable xn where var=n.
* @param var The "index" of the variable.
* @return The Value of the variable.
*/
int while_getVariable(int var);

/**
* Runs a WHILE-Program.
* @param The string that contains the WHILE-Program.
*/
int while_run(char * program);

#endif
